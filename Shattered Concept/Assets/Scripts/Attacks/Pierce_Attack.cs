﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pierce_Attack : MonoBehaviour 
{	
	//These need to be grabbed from the respective stats libraries.
	//Defending entitiy's stats
	private static int dodgeMin = 0;
	private static int dodgeMax = 10;
	private static int retaliation = 30;
	private static int healthMax = 20;
	private int health = 20;
	private static int auraMax = 100;
	private int aura = 100; // This needs to be a moving variable preserved between fights. Health too.
	private static int armor = 10;

	//Defending entity's strengths/weaknesses
	private static bool weaktoSlash = false;
	private static bool weakToPierce = false;
	private static bool weakToCrush = true;
	private static bool strongToSlash = true;
	private static bool strongToPierce = false;
	private static bool strongToCrush = false;

	//These are pretty self-explanatory.
	private bool dead = false;
	private bool uncon = false;

	//Attacking entity's stats
	private static int accuracy = 80;
	private static int crit = 5;
	private static int critBonus = 2; //crit bonus may simply be x2 across the board.

	//private static int rngPenaltyMin = 15;
	//private static int rngPenaltyMax = 20;
	//Ranged attacks are going to be in a different script, saved for reference.

	//Attacking entity's weapon's stats
	private int damageMin = 30;
	private int damageMax = 40;
	private int armorPierceMin = 10;
	private int armorPierceMax = 15;

	//private int rngPenaltyRand = Random.Next(rngPenaltyMin, rngPenaltyMax);
	//More reference stuff.

	public string ifHit = "";
	public string ifKO = "";
	public string ifKill = "";
	public string ifWeak = "";
	public string damagePrint = "";

	void Start ()
	{
		//These all define the number to be added to the minimums of the respective variables. It'll pick a random number between 0 and the difference of the minimum and maximums.
		int dodgeRand = Random.Range(0, dodgeMax - dodgeMin);
		int retal = Random.Range(0, retaliation); //The "retaliation" variable describes a percentage chance. 0% through whatever%
		int damageRand = Random.Range(0, damageMax - damageMin);
		int armorPierceRand = Random.Range(0, armorPierceMax - armorPierceMin);

		//These are the percentanges percent chances will be compared to. For example, if "retal" ends up greater than or equal to "retalChance" a retaliatory attack is made. "chance" is what the hit and crit chances are compared to.
		int chance = Random.Range(0, 100);
		int retalChance = Random.Range(0, 100);


		int dodge = dodgeMin + dodgeRand;
		int armorTemp = armor; //Don't want to fuck with variables meant to be static relative to the function that's calling them? Temporary variables are a coooool fix, dude.
		int accuracyTemp = accuracy;
		int auraTemp = aura;
		int critTemp = crit;



	if (uncon == true) // if someone's knocked out, it pretty hard for them to dodge or ficht back. Easier target too.
	{
		dodge = 0; //make sure "dodge" is defined BEFORE this bit. "retal" too.
		accuracyTemp = accuracyTemp * 2;
			if (accuracyTemp > 100) 
			{
				accuracyTemp = 100; //100% chance to hit is the max. I don't like variables going outside of their expected/intended ranges.
			}
		critTemp = critTemp * 2;
		retal = 0;
	}

		accuracyTemp = accuracyTemp - dodge; //Doing dodge this way makes sure that even if an attack manages 100% chance to hit, it can still be dodged.

	//if (attackRanged == true)
	//{
	//	accuracy = accuracy - rngPenaltyMin + rngPenaltyRand;
	//	retal = 0;
	//}
	//More ranged reference for later.

	int damage = damageMin + damageRand; //This has to be defined before any bits modifying the damage variable. Crit and attack type weakness for the most part.

	if (weakToPierce == true)
	{
		damage = damage * 2;
			ifWeak = "Armor Pierced!";
		//Gotta assign these to a text element later. Like, when there's even a fucking placeholder map in the project.
	}

	if (strongToPierce == true)
	{
		damage = damage / 2;
			ifWeak = "Resist!";
	}

	int armorPierce = armorPierceMin + armorPierceRand;
	armorTemp = armorTemp - armorPierce;

	if (armorTemp > 0)
	{
		armorTemp = 0; //Don't want there to be bonus damage if the armor pierce is greater than the armor
	}

	damage = damage - armorTemp;


	if (accuracy >= chance)
	{
		if (crit < chance) //Gotta make sure it's only possible to crit if the attack lands first.
		{
				ifHit = "Hit";
				damagePrint = "" + damage;

				if (weakToPierce == true) //Putting these in here so the resist/weakness text will only play if the attack hits.
				{
					damage = damage * 2;
					ifWeak = "Armor Pierced!";
				}
				if (strongToPierce == true)
				{
					damage = damage / 2;
					ifWeak = "Resist!";
				}
		}

			if (crit >= chance) 
			{
				ifHit = "Hit";
				damage = damage * critBonus;
				damagePrint = damage + "!";
				if (weakToPierce == true)
				{
					damage = damage * 2;
					ifWeak = "Armor Pierced!";
				}

				if (strongToPierce == true)
				{
					damage = damage / 2;
					ifWeak = "Resist!";
				}
			}
				
	}

		if (chance > accuracy)
		{
			damage = 0;
			ifHit = "Miss!";
		}
 
		if (damage > 0)
		{
			damage = 0; //Don't want to heal for negative damage.
		}

	aura = aura - damage;
	if (aura > 0)
	{
		aura = 0; //No negative aura either.
	}
	
	damage = damage - auraTemp;

	if (aura <= 0)
	{
		health = health - damage;
	}

	if (health < 1)
	{
		uncon = true;
			ifKO = "KO!";
	}
	
	if (health <= -10)
	{
		dead = true;
		ifKill = "Killshot!";
		health = -10;
	}
			
	if (retal > retalChance)
	{
		//retalAttack(soldier, grimm);
		// Gotta make the retal attack script.
	}
	
}
}


