﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Amaria_DEF_Stats : MonoBehaviour 
{

	private int aura = 100;
	private int dodgeMin = 5;
	private int dodgeMax = 10;
	private int healthMax = 20;
	private int retal = 20;

	private bool weakToCrush = true;
	private bool weakToSlash = false;
	private bool weakToPierce = false;
	private bool strongToCrush = false;
	private bool strongToSlash = true;
	private bool strongToPierce = false;
		
}
