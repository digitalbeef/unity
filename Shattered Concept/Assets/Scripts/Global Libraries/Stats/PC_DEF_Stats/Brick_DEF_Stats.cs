﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick_DEF_Stats : MonoBehaviour {

	private int aura = 120;
	private int dodgeMin = 0;
	private int dodgeMax = 0;
	private int healthMax = 30;
	private int retal = 30;

	private bool weakToCrush = true;
	private bool weakToSlash = true;
	private bool weakToPierce = false;
	private bool strongToCrush = false;
	private bool strongToSlash = false;
	private bool strongToPierce = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
