﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miles_Def_Stats : MonoBehaviour {

	private int aura = 150;
	private int dodgeMin = 5;
	private int dodgeMax = 10;
	private int healthMax = 30;
	private int retal = 20;

	private bool weakToCrush = false;
	private bool weakToSlash = false;
	private bool weakToPierce = false;
	private bool strongToCrush = true;
	private bool strongToSlash = true;
	private bool strongToPierce = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
