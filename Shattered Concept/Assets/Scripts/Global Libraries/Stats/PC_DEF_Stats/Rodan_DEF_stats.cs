﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rodan_DEF_stats : MonoBehaviour {

	private int aura = 100;
	private int dodgeMin = 5;
	private int dodgeMax = 10;
	private int healthMax = 20;
	private int retal = 40;

	private bool weakToCrush = true;
	private bool weakToSlash = false;
	private bool weakToPierce = false;
	private bool strongToCrush = false;
	private bool strongToSlash = true;
	private bool strongToPierce = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
